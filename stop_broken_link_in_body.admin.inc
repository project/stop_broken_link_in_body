<?php

/**
 * @file
 * Contains administrative configuration page.
 */

/**
 * This function provides a form to admin interface.
 */
function stop_broken_link_in_body_admin_setting_form() {
  // Get List Of Content Type.
  $node_type = node_type_get_names();

  // Define Form Field for linkchecker configuration.
  $form['stop_broken_link_in_body']['stop_broken_link_in_body_node_types'] = array(
    '#default_value' => is_array(variable_get('stop_broken_link_in_body_node_types')) ?
    array_filter(variable_get('stop_broken_link_in_body_node_types')) : array(NULL),
    '#options' => $node_type,
    '#type' => 'checkboxes',
    '#title' => t('List of content type in which you want stop broken link'),
    '#description' => t('Select Content type which you want to stop broken link in body.'),
  );
  $form['stop_broken_link_in_body']['stop_broken_link_in_body_restrict_the_number'] = array(
    '#default_value' => variable_get('stop_broken_link_in_body_restrict_the_number', 30),
    '#type' => 'textfield',
    '#title' => t("Configuration to restrict the number of links to validate"),
    '#description' => t('Add the value to restrict the number of links to validate.'),
    '#element_validate' => array('element_validate_integer_positive'),
  );
  return system_settings_form($form);
}
