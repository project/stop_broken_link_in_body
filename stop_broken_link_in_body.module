<?php

/**
 * @file
 * Defines url for all the static files "stop_broken_link_in_body".
 */

/**
 * Implements hook_menu().
 *
 * This function add a menu item to admin menus for our module configurations.
 */
function stop_broken_link_in_body_menu() {
  $items = array();
  $items['admin/config/system/stop-broken-link-in-body'] = array(
    'title' => 'Stop Broken link in Body',
    'description' => 'Stop broken link in body field.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('stop_broken_link_in_body_admin_setting_form'),
    'access callback' => 'user_access',
    'access arguments' => array('administer stop broken link in body'),
    'file' => 'stop_broken_link_in_body.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function stop_broken_link_in_body_form_node_form_alter(&$form, $form_state) {
  // Validate content type.
  if (variable_get('stop_broken_link_in_body_node_types') != "") {
    if (in_array($form_state['node']->type, array_values(array_filter(variable_get('stop_broken_link_in_body_node_types'))))) {
      $form['#validate'][] = 'stop_broken_link_in_body_validate';
    }
  }
}

/**
 * Validation handler for node form.
 */
function stop_broken_link_in_body_validate($form, &$form_state) {
  if (isset($form_state['values'])) {

    // Handle multiple body fields.
    foreach ($form_state['values']['body'][LANGUAGE_NONE] as $val) {
      $bodyfield = $val['value'];
      $bodyurls = stop_broken_link_in_body_get_urls($bodyfield);
      $hrefurl = stop_broken_link_in_body_hrefget_urls($bodyfield);

      // Get links from body text.
      $urls = array_unique(array_merge($bodyurls, $hrefurl));
      $restrict_number = variable_get('stop_broken_link_in_body_restrict_the_number');

      // Check for multiple urls.
      if ($restrict_number < count($urls)) {
        form_set_error($restrict_number, t('Max urls allowed is %count', array('%count' => $restrict_number)));
      }
      else {
        // Handle multiple urls.
        foreach ($urls as $url) {
          // Add base_url if not already added.
          $res = drupal_http_request(stop_broken_link_in_body_add_base_url($url));
          $status = $res->code;
          if ($status != 200) {
            form_set_error($status, t('Error on these below url!'));
            drupal_set_message(t('Link check of @url status code: @code', array('@url' => $url, '@code' => $status)), 'error');
          }
        }
      }
    }
  }
}

/**
 * Implements hook_help().
 */
function stop_broken_link_in_body_help($path, $arg) {
  switch ($path) {
    case 'admin/config/system/stop-broken-link-in-body':
      return t('Set the basic Configuration to content type in which you want stop broken link');

    case 'admin/help#stop_broken_link_in_body':
      $output = '';
      $output .= '<p>' . t('The Stop broken link in body module detects defunct links from your content right when the content is being created. While user creates a node, the module detects broken links in the contents body by checking the remote sites and evaluating the HTTP response codes. It shows all broken links in the body section and on the content edit page, if a link check fails. If a broken link is needed, the content cannot be saved.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_permission().
 */
function stop_broken_link_in_body_permission() {
  return array(
    'administer stop broken link in body' => array(
      'title' => t('Administer stop broken link in body'),
      'description' => t('allow config to stop broken link in body.'),
    ),
  );
}

/**
 * Check absolute urls, if not found then converts them into absolute url.
 *
 * @param string $url
 *   Contains url string $url.
 *
 * @return string
 *   Absolute url.
 */
function stop_broken_link_in_body_add_base_url($url) {
  global $base_url;
  if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
    $url = $base_url . $url;
  }
  return $url;
}

/**
 * Check body field text to find for urls and converts them into absolute urls.
 *
 * @param string $string
 *   Contains body field strings $string.
 *
 * @return string
 *   Absolute url.
 */
function stop_broken_link_in_body_get_urls($string) {
  $regex = '/https?\:\/\/[^\" ]+/i';
  preg_match_all($regex, $string, $matches);
  return ($matches[0]);
}

/**
 * Check body field text to find ahref and then convert them into absolute urls.
 *
 * @param string $string
 *   Contains body field strings $string.
 *
 * @return string
 *   Absolute url string.
 */
function stop_broken_link_in_body_hrefget_urls($string) {
  $regex = '/href=["\']?([^"\'>]+)["\']?/';
  preg_match_all($regex, $string, $matches);
  return ($matches[1]);
}
